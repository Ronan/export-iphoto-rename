Export iPhoto Rename Tool
=========================

Changes the name of the moment directory so that they will be easily sortable by date.
Applies to all photos in the enclosed directory.

"1 February 2017" => "2017 02 01"
"Varosliget - Budapest XIV. kerület, 24 April 2016" => "2016 04 24 - Varosliget - Budapest XIV. kerület"

Also deduplicate files.
