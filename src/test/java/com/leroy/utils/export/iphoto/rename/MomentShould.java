package com.leroy.utils.export.iphoto.rename;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

public class MomentShould {

    private static final String ROOT_FOLDER = "/Users/rleroy/Library/Lima/Pictures/iPhoto/";
    
    @Test
    public void retreive_a_date_from_a_date_name() {
        Moment moment = new Moment(Paths.get(ROOT_FOLDER + "1 April 2015"));
        
        Assert.assertEquals(LocalDate.of(2015, 04, 01), moment.getDate());
    }
    
    @Test
    public void retreive_a_date_from_a_place_and_date_name() {
        Moment moment = new Moment(Paths.get(ROOT_FOLDER + "Beijing, Beijing - Great Wall of China, 24 November 2012"));
        
        Assert.assertEquals(LocalDate.of(2012, 11, 24), moment.getDate());
    }
    
    @Test
    public void rename_a_moment() {
        Moment moment = new Moment(Paths.get(ROOT_FOLDER + "Beijing, Beijing - Great Wall of China, 24 November 2012"));
        
        Assert.assertEquals(
            "20121124 - Beijing, Beijing - Great Wall of China, 24 November 2012",
            moment.getName()
        );
    }
    
    @Test
    public void not_rename_twice() {
        Moment moment = new Moment(Paths.get(ROOT_FOLDER + "20121124 - Beijing, Beijing - Great Wall of China, 24 November 2012"));
        
        Assert.assertEquals(
            "20121124 - Beijing, Beijing - Great Wall of China, 24 November 2012",
            moment.getName()
        );
    }
    
    @Test
    public void change_moment_file_name() {
        Moment moment = new Moment(Paths.get(ROOT_FOLDER + "Beijing, Beijing - Great Wall of China, 24 November 2012"));
        
        moment.rename();
        
        Assert.assertTrue(Paths.get(moment.getName()).toFile().exists());
        Assert.assertTrue(Paths.get(moment.getName()).toFile().isDirectory());
    }
    
    @Test
    public void retreive_dates_from_moments() throws IOException {
        Files.list(Paths.get(ROOT_FOLDER))
            .map(p -> new Moment(p))
            .map(m -> m.getName())
            .sorted()
            .forEach(System.out::println);
    }
}
