package com.leroy.utils.export.iphoto.rename;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public class Moment {

    private Path path;
    
    public Moment(Path path) {
        this.path = path;
    }

    public LocalDate getDate() {
        String[] splitted = path.toFile().getName().split(" ");
        return LocalDate.of(
            Integer.parseInt(splitted[splitted.length-1]), 
            Month.valueOf(splitted[splitted.length - 2].toUpperCase()), 
            Integer.parseInt(splitted[splitted.length - 3])
        );
    }

    public String getName() {
        String res = this.getFileName();

        String date = getDate().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        if (!this.getFileName().startsWith(date)) {
            res = date + " - " + this.getFileName();
        }
        return res;
    }
    
    private String getFileName() {
        return path.toFile().getName();
    }

    public void rename() {
        File source = path.toFile();
        File target = path.getParent().resolve(this.getName()).toFile();
        if (!target.exists()) {
            System.out.println("rename " + source + " to " + target);
            source.renameTo(target);
        } else if (!target.equals(source)){
            System.out.println(target + " exists, sync files");
            try {
                Files.list(source.toPath())
                    .map(Path::toFile)
                    .forEach(f -> syncTo(f, target.toPath()));
                Files.delete(source.toPath());
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }
    }
    
    private void syncTo(File source, Path targetDir) {
        try {
            if (!targetDir.resolve(source.getName()).toFile().exists()) {
                System.out.println("move " + source + " into " + targetDir);
                Files.move(source.toPath(), targetDir.resolve(source.getName()));
            } else {
                System.out.println(source + " already exists in "+ targetDir);
                Files.delete(source.toPath());
            }
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }
    
}
