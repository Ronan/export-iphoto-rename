package com.leroy.utils.export.iphoto.rename;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class App {
    
    private static final String ROOT_FOLDER = "/Users/rleroy/Library/Lima/Pictures/iPhoto/";

    public static void main(String...args) throws IOException {
        App app = new App(ROOT_FOLDER);
        app.renameAll();
        app.deduplicate();
        app.countAll();
    }
    
    private String root;
    
    public App(String root) {
        this.root = root;
    }

    private void renameAll() throws IOException {
        Files.list(Paths.get(this.root))
            .map(p -> new Moment(p))
            .forEach(m -> m.rename());
    }

    private void deduplicate() throws IOException {
        Files.list(Paths.get(this.root))
            .flatMap(p -> this.list(p))
            .map(p -> p.toFile())
            .filter(f -> f.getName().contains("("))
            .forEach(f -> deduplicate(f));
    }

    private void deduplicate(File f) {
        String name = f.getName().replaceAll(" \\(.\\)", "");
        File target = f.toPath().getParent().resolve(name).toFile();
        System.out.println("target " + target.getAbsolutePath());
        if (target.exists()) {
            System.out.println("remove " + f.getAbsolutePath());
            f.delete();
        } else {
            System.out.println("keep " + f.getAbsolutePath());
        }
    }

    private long countAll() throws IOException {
        long sum = Files.list(Paths.get(this.root))
            .mapToLong(this::count)
            .sum();
        System.out.println("countAll : " + sum);
        return sum;
    }
    
    private long count(Path path) {
        return this.list(path).count();
    }

    private Stream<Path> list(Path path) {
        try {
            return Files.list(path);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

}
